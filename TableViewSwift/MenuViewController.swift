//
//  MenuViewController.swift
//  TableViewSwift
//
//  Created by Deepak Agarwal on 28/04/16.
//  Copyright © 2016 Deepak Agrawal. All rights reserved.
//

import UIKit



class MenuViewController: UITableViewController {
    
    var menuItems = [String]()
    
    var mainViewController: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        menuItems = ["Home", "Red", "Green", "Blue"]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
        cell.textLabel?.text = menuItems[indexPath.row]
        
        return cell
        
    }
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        if indexPath.row == 0  {
            
            let mainViewController:UIViewController = storyboard.instantiateViewControllerWithIdentifier("mainViewController")
            self.slideMenuController()?.changeMainViewController(UINavigationController(rootViewController: mainViewController), close: true)
            
            
        }
        else if indexPath.row == 1 {
            
                let redViewController:UIViewController = storyboard.instantiateViewControllerWithIdentifier("redViewController")
            self.slideMenuController()?.changeMainViewController(UINavigationController(rootViewController: redViewController), close: true)
            
        }
        else if indexPath.row == 2 {
            
            let redViewController:UIViewController = storyboard.instantiateViewControllerWithIdentifier("greenViewController")
            self.slideMenuController()?.changeMainViewController(UINavigationController(rootViewController: redViewController), close: true)
            
        }
        else if indexPath.row == 3 {
            
            let redViewController:UIViewController = storyboard.instantiateViewControllerWithIdentifier("blueViewController")
            self.slideMenuController()?.changeMainViewController(UINavigationController(rootViewController: redViewController), close: true)
            
        }
        
    }
    
    
    

}
