//
//  RedViewController.swift
//  TableViewSwift
//
//  Created by Deepak Agarwal on 28/04/16.
//  Copyright © 2016 Deepak Agrawal. All rights reserved.
//

import UIKit

class RedViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.addLeftBarButtonWithImage(UIImage(named: "menu_icon")!)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
