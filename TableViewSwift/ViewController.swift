//
//  ViewController.swift
//  TableViewSwift
//
//  Created by Deepak Agarwal on 26/04/16.
//  Copyright © 2016 Deepak Agrawal. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.addLeftBarButtonWithImage(UIImage(named: "menu_icon")!)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 90.0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("customCell", forIndexPath: indexPath) as? CustomCell
        if indexPath.row == 1 {
            
            cell?.label1.text = String(indexPath.row)
            cell?.label2.text = " To implement a container view controller, you must establish relationships between your view controller and its child view controllers. Establishing these parent-child relationships is required before you try to manage the views of any child view controllers."
            cell?.label3.text = "To incorporate a child view controller into your content programmatically.To incorporate a child view controller into your content programmatically.To incorporate a child view controller into your content programmatically.To incorporate a child view controller into your content programmatically."
            
           // cell?.label2.frame.size.height =
            
        }
        else if indexPath.row == 2 {
            
            cell?.label1.text = String(indexPath.row)
            cell?.label2.text = " To implement a container view controller, you must establish relationships between your view controller and its child view controllers. Establishing these parent-child relationships is required before you try to manage the views of any child view controllers. Doing so lets UIKit know that your view controller is managing the size and position of the children. You can create these relationships in Interface Builder or create them programmatically. When creating parent-child relationships programmatically, you explicitly add and remove child view controllers as part of your view controller setup."
            cell?.label3.text = "To incorporate a child view controller into your content programmatically, create a parent-child relationship between the relevant view controllers by doing the following:"
            
        }
        else
        {
            cell?.label1.text = String(indexPath.row)
            cell?.label2.text = String(indexPath.row)
            cell?.label3.text = String(indexPath.row)
        }
        
        return cell!
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if indexPath.row == 1 {
            
            let label1 = "1"
            let label2 = " To implement a container view controller, you must establish relationships between your view controller and its child view controllers. Establishing these parent-child relationships is required before you try to manage the views of any child view controllers."
            let label3 = "To incorporate a child view controller into your content programmatically.To incorporate a child view controller into your content programmatically.To incorporate a child view controller into your content programmatically.To incorporate a child view controller into your content programmatically."
            
            return label1.heightWithConstrainedWidth(self.view.frame.size.width-40, font: UIFont.systemFontOfSize(18.0)) + label2.heightWithConstrainedWidth(self.view.frame.size.width-40, font: UIFont.systemFontOfSize(18.0)) + label3.heightWithConstrainedWidth(self.view.frame.size.width-40, font: UIFont.systemFontOfSize(18.0)) + 26
            
            
        }
        else if indexPath.row == 2 {
            
            let label1 = "2"
            let label2 = "  To implement a container view controller, you must establish relationships between your view controller and its child view controllers. Establishing these parent-child relationships is required before you try to manage the views of any child view controllers. Doing so lets UIKit know that your view controller is managing the size and position of the children. You can create these relationships in Interface Builder or create them programmatically. When creating parent-child relationships programmatically, you explicitly add and remove child view controllers as part of your view controller setup."
            let label3 = "To incorporate a child view controller into your content programmatically, create a parent-child relationship between the relevant view controllers by doing the following:"
             return label1.heightWithConstrainedWidth(self.view.frame.size.width-40, font: UIFont.systemFontOfSize(18.0)) + label2.heightWithConstrainedWidth(self.view.frame.size.width-40, font: UIFont.systemFontOfSize(18.0)) + label3.heightWithConstrainedWidth(self.view.frame.size.width-40, font: UIFont.systemFontOfSize(18.0)) + 26
            
        }
        else {
            return 90.0;
        }
 
    }

}

extension String {
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.max)
        
        let boundingBox = self.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return boundingBox.height
    }
}



